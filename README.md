# GPF DevSecOps Knowledge base

ยินดีต้อนรับเข้าสู่ศูนย์ข้อมูลด้าน DevSecOps ของกองทุนบำเหน็ขบำนาญข้าราชการ

# สารบัญ

#### DevSecOps Team Culture Role Responsibilities
1. [วัฒนธรรม DevSecOps (DevSecOps Culture)](../../../wikis/DevSecOps-Culture)
2. [หลักการ Shift Left and Shift Right](../../../wikis/Shift-Left-and-Shift-Right)
3. [DevSecOps Team Culture Roles Responsibilities]()\
    3.1. [Team](../../../wikis/Team)\
	3.2. [Roles and Responsibilities]()\
		3.2.1. [Developer Team](../../../wikis/Developer-Team)\
		3.2.2. [QA Tester](../../../wikis/QA-Tester)\
		3.2.3. [DevOps Engineer](../../../wikis/DevOps-Engineer)\
		3.2.4. [IT Operation Team](../../../wikis/IT-Operation-Team)\
		3.2.5. [Platform Engineer](../../../wikis/Platform-Engineer)\
		3.2.6. [Database Administrator](../../../wikis/Database-Administrator)\
		3.2.7. [24/7 Support Engineer](../../../wikis/24/7-Support-Engineer)\
		3.2.8. [Security Team](../../../wikis/Security-Team)
    
--------------------------------------------------------------------------
#### ข้อมูลการวิเคราะห์ประเมิน แนะนำ เสริมข้อมูล เพื่อปรับเพิ่มประสิทธิภาพของกระบวนการ DevSecOps ของ กบข.
*ข้อมูลนำเสนอวันที่ 7 พย. 2565*
1. [Secure Pipeline Practice](../../../wikis/GPF-Secure-Pipeline-Practice)
2. [CI CD Pipeline ตามมาตรฐานสากล โดยแนะนำจาก INFINITY](../../../wikis/CI-CD-pipeline-recommend-by-INFINITY)
3. [CD Pipeline OnPrem ขึ้น OnCloud (AZURE) ปัจจุบันของ กบข.](../../../wikis/Existing-Pipeline-OnPrem-to-AZURE)
4. [วิเคราะห์กระบวนการ DevOps CI/CD Pipeline  ปัจจุบันของ กบข.](../../../wikis/analyze-GPF-Existing-CICD)\
    4.1. [วิเคราะห์ข้อดีและข้อด้อยของ Jenkins กบข.](../../../wikis/analyze-Pros-Cons-Jenkins)\
    4.2. [วิเคราะห์ข้อดีและข้อด้อยของ Azure Pipelines กบข.](../../../wikis/analyze-Pros-Cons-AZURE-Pipeline)\
    4.3. [วิเคราะห์การใช้งาน Git Repositorys กบข.](../../../wikis/analyze-Pros-Cons-Git-Repo)\
    4.4. [วิเคราะห์แต่ละขั้นตอน (Stage) ของแต่ละ Pipeline ณ ปัจจุบันของ กบข. กบข.](../../../wikis/analyze-GPF-CICD-Stage)
5. [คำแนะนำเพิ่ม Security ให้กับ DevOps CI/CD Pipeline กบข.](../../../wikis/Improve-Security-GPF-DevOps-CICD)\
	5.1. [วิเคราะห์ แนะนำการใช้งาน GitLab และ Jenkins](../../../wikis/analyze-Recommend-GitLab-Jenkins)\
	5.2. [วิเคราะห์ แนะนำด้านความปลอดภัยของ Credential](../../../wikis/Analyze-Recommend-Credential)\
	5.3. [วิเคราะห์ แนะนำด้านการใช้งาน Jenkins กับ Kubernetes เพื่อสร้าง Slave node](../../../wikis/Analyze-Recommend-Jenkins-K8s-for-slave)\
	5.4. [วิเคราะห์ แนะนำการจัดวาง Folder ของ Jenkins](../../../wikis/Analyze-Recommend-Jenkins-Folder)\
	5.5. [คำแนะนำการทำ Jenkinsfile ให้เป็น Template](../../../wikis/Analyze-Recommend-Jenkinsfile-template)\
	5.6. [อธิบาย CI Pipeline ที่แนะนำ](../../../wikis/Recommend-CI-Pipeline)\
	5.7. [อธิบาย CD Pipeline ที่แนะนำ](../../../wikis/Recommend-CD-Pipeline)\
	5.8. [อธิบาย Release Pipeline ที่แนะนำ](../../../wikis/Recommend-Release-Pipeline)\
	5.9. [คำแนะนำการทำ Pipeline Monitoring](../../../wikis/Recommend-Pipeline-Monitoring)\
		5.9.1. [ตัวอย่างรูป Grafana Dashboard Pipeline Monitoring ข้อมูลวันที่ 7 สิงหาคม 2566](../../../wikis/Grafana-Dashboard-Pipeline)
6. [บทวิเคราะห์ แนะนำ และเสริมข้อมูลในกระบวนการ DevSecOps ของ กบข.](../../../wikis/Analyze-for-Improve-GPF-DevSecOps)\
	6.1. [ผลการวิเคราะห์ แนะนำ ในกระบวนการ Plan](../../../wikis/CICD-Plan)\
		6.1.1. [Threat Modeling](../../../wikis/CICD-Threat-Modeling)\
	6.2. [ผลการวิเคราะห์ แนะนำ ในกระบวนการ Code](../../../wikis/CICD-Code)\
		6.2.1. [IDE Security Plugin](../../../wikis/IDE-Security-Plugin)\
		6.2.2. [Peer Review และ Secure Coding Standards](../../../wikis/Secure-Coding-Standards)\
		6.2.2. [Secure Pipeline](../../../wikis/Secure-Pipeline)\
		6.2.2.1. [แนวปฏิบัติ Secure Pipeline Guideline (ข้อมูลวันที่ 7 สิงหาคม 2566)](../../../wikis/Secure-Pipeline-Guideline)\
	6.3. [ผลการวิเคราะห์ แนะนำ ในกระบวนการ Build](../../../wikis/Build)\
		6.3.1. [Static Application Security Testing (SAST)](../../../wikis/SAST)\
		6.3.2. [Software Composition Analysis (SCA)](../../../wikis/SCA)\
		6.3.3. [Signing Code and Artifacts](../../../wikis/Signing-Code-and-Artifacts)\
	6.4. [ผลการวิเคราะห์ แนะนำ ในกระบวนการ Test](../../../wikis/Test)\
		6.4.1. [Dynamic Application Security Testing (DAST)](../../../wikis/DAST)\
		6.4.2. [Cloud Configuration Validation and Infrastructure Scanning](../../../wikis/IaC-scan)\
		6.4.3. [Container Scanning](../../../wikis/Container-Scanning)\
	6.5. [ผลการวิเคราะห์ แนะนำ ในกระบวนการ Release](../../../wikis/Release)\
	6.6. [ผลการวิเคราะห์ แนะนำ ในกระบวนการ Deploy](../../../wikis/Deploy)\
		6.6.1. [Progressive Delivery](../../../wikis/Progressive-Delivery)\
		6.6.2. [Credential Handover](../../../wikis/Credential-Handover)\
	6.7. [ผลการวิเคราะห์ แนะนำ ในกระบวนการ Operate](../../../wikis/Operate)\
		6.7.1. [Penetration Testing](../../../wikis/Penetration-Testing)\
		6.7.2. [Threat Intelligence](../../../wikis/Monitoring)\
	6.8. [ผลการวิเคราะห์ แนะนำ ในกระบวนการ Monitoring](../../../wikis/Monitoring)\
		6.8.1. [Pipeline Monitoring](../../../wikis/Pipeline-Monitoring)\
		6.8.2. [Centralised Logging and Monitoring](../../../wikis/Centralised-Logging-and-Monitoring)
7. [แผนการเพิ่มประสิทธิภาพด้าน Security ให้กับทาง กบข.]()\
	7.1.	[วางแผนระยะเริ่มต้นในช่วงปี 2022 - 2024 ของการดำเนินการด้าน DevSecOps](../../../wikis/GPF-DevSecOps-plan-2022-2023)\
	7.2.	[วางแผนในช่วงปี 2023 - 2024 ของการดำเนินการด้าน DevSecOps](../../../wikis/GPF-DevSecOps-plan-2023-2024)


--------------------------------------------------------------------------
#### Presentation
1. [Threat Modeling Workshop workshop](./SlidePresentation/Threat_Modeling_Workshop_-_For_GPF_workshop.pdf)
2. [GPF DevOps Technology Stack](../../../wikis/GPF-DevOps-Technology-Stack)
3. [อัพเดทแผนปรับปรุง DevSecOps 2023 DSO1-4(ข้อมูลวันที่ 20 มิถุนายน 2566)](./SlidePresentation/หารือ_DevSecOps_2023_DSO4_20June2023_.pdf)

--------------------------------------------------------------------------
#### Documentation
1. [Case Study Security Control in DevSecOps](../../../wikis/DSO-Case-Study-Security-Control)
2. [Example Security Exemption form](./Document/Example_Security_Exemption_form.docx)

--------------------------------------------------------------------------
#### Code/Example
1. [Example Jenkinsfile integrated Fortify SAST](./Code/Pipeline/fortify-full-integrated_pipeline_with_comment)


--------------------------------------------------------------------------

